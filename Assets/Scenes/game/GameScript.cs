﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameScript : MonoBehaviour
{
    public float zombiesPerWave = 1f;

    public Transform[] ZombieSpawners;
    public GameObject Zombie;

    private float wave = 1f;
    private bool waveIsLoading = true;

    // Start is called before the first frame update
    void Start()
    {
        startGame();
    }
     
    // Update is called once per frame
    void Update()
    {
        if (GameObject.Find("Zombie(Clone)") == null && !waveIsLoading)
        {
            waveIsLoading = true;
            StartCoroutine(newWave());
        }
    }

    public void startGame()
    {
        showWave();
        StartCoroutine(spawnZombies());

    }

    public IEnumerator newWave ()
    {
        WaitForSeconds wait = new WaitForSeconds(10f);
        yield return wait;
        wave++;
        showWave();
        StartCoroutine(spawnZombies());
    }

    public IEnumerator spawnZombies() 
    {
        WaitForSeconds wait = new WaitForSeconds(Random.Range(1f, 4f));

        float zombieanz = wave * zombiesPerWave;

        for (float i = 1; i <= zombieanz; i++) {
            int randomSpawner = Mathf.RoundToInt(Random.Range(0f, ZombieSpawners.Length-1f));
            GameObject ZombieClone = Instantiate(Zombie, ZombieSpawners[randomSpawner].transform.position, ZombieSpawners[randomSpawner].transform.rotation) as GameObject;
            ZombieClone.SetActive(true);
            if(i == zombieanz) waveIsLoading = false;
            yield return wait;
        }
    }

    public void showWave()
    {
        transform.GetChild(0).Find("CamOverlay").Find("Wave").GetComponent<UnityEngine.UI.Text>().text = "Wave " + wave.ToString();
        transform.GetChild(0).Find("CamOverlay").Find("Wave").GetComponent<Animation>().Play("schowWave");
    }
}
