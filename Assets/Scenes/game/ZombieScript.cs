﻿using UnityEngine;

public class ZombieScript : MonoBehaviour
{
    public Camera Player;
    public GameObject[] Powerups;

    private float dmg = 50;
    private float health = 200f;
    //declare the transform of our goal (where the navmesh agent will move towards) and our navmesh agent (in this case our zombie)
    private Transform goal;
    private UnityEngine.AI.NavMeshAgent agent;
    private bool isDead = false;
    private float attackRange = 2.3f;

    void Update()
    {
        if (!isDead)
        {
            float dist = Vector3.Distance(Player.transform.position, gameObject.transform.position);
            if (dist <= attackRange && !GetComponent<Animation>().IsPlaying("attack")) attack();
            if (dist > attackRange && !GetComponent<Animation>().IsPlaying("attack")) walk();
        }
    }

    void walk()
    {
        //create references
        goal = Player.transform;
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        //set the navmesh agent's desination equal to the main camera's position (our first person character)
        agent.destination = goal.position;
        //start the walking animation
        GetComponent<Animation>().Play("walk_in_place");
    }

    void attack()
    {
        //stop the zombie from moving forward by setting its destination to it's current position
        agent.destination = gameObject.transform.position;
        GetComponent<Animation>().Stop();
        //stop the walking animation and play attack animation
        GetComponent<Animation>().Play("attack");
        //do Dmg to Player
        PlayerScript player = Player.transform.root.gameObject.GetComponent<PlayerScript>();
        if (player != null)
        {
            player.TakeDmgToPlayer(dmg);
        }
    }

    public void TakeDmg(float amount, string part)
    {
        PlayerScript player = Player.transform.root.gameObject.GetComponent<PlayerScript>();
        if (player != null) player.AddCoins(part == "Bip01 Head");

        if (part == "Bip01 Head") health = 0;
        else health -= amount;

        if (health <= 0 && !isDead) Die();
    }

    void Die()
    {
        //stop the walking animation and play attack animation
        GetComponent<Animation>().Stop();
        isDead = true;
        //stop the zombie from moving forward by setting its destination to it's current position
        agent.destination = gameObject.transform.position;
        //chance to drop a Powerup
        if (Random.value > 0) //%100 percent chance (1 - 0.2 is 0.8)
        {
            Vector3 pos = transform.position;
            pos.y += 1f;
            int randomPowerup = Mathf.RoundToInt(Random.Range(2f, Powerups.Length-1f));
            Instantiate(Powerups[randomPowerup], pos, transform.rotation);
        }
        //destroy dead Body after 4 sec
        Destroy(gameObject, 4f);
    }
}