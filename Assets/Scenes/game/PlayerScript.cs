﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerScript : MonoBehaviour
{
    public float munitionPowerupBonus = 50f;
    public float health = 100f;
    public float maxHealth = 100f;
    public int coinsOnHit = 10;
    public int coinsOnHeadshot = 60;
    public bool isNearWeapon = false;
    public string nearWeapon = "";

    private bool isDead = false;
    private int coins = 500000;
    private string activeWeapon = "";
    private GameObject RandomWeaponBox = null;
    private bool isNearRandomWeaponBox = false;
    private string nearDrink = "";
    private bool JuggerNog = false;
    private bool DoubleTap = false;
    private bool QuickRevive = false;

    void Start(){
        UpdateCoins();
    }

    void Update (){
        activeWeapon = GameObject.Find("Weapon").GetComponent<WaffenScript>().getActiveWeapon();
        if (Input.GetKeyDown(KeyCode.X) && isNearWeapon)
        {
            Debug.Log(isNearRandomWeaponBox); 
            WaffenScript player = GameObject.Find("Weapon").GetComponent<WaffenScript>();
            player.changeWeapon(nearWeapon, !isNearRandomWeaponBox);
            if(isNearRandomWeaponBox && RandomWeaponBox) {
                if(RandomWeaponBox.GetComponent<Animation>().IsPlaying("OpenWeaponBox")) {
                    RandomWeaponBox.GetComponent<Animation>()["OpenWeaponBox"].time = 9.9f;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.X) && RandomWeaponBox != null && !RandomWeaponBox.GetComponent<Animation>().IsPlaying("OpenWeaponBox")){
            bool priceCheck = DecCoins(1250);
            if(priceCheck){
                gameObject.transform.GetChild(0).Find("CamOverlay").Find("Buy").GetComponent<UnityEngine.UI.Text>().enabled = false;
                RandomWeaponBox.GetComponent<RandomWeaponBox>().openBox();
            }
        }
        if (Input.GetKeyDown(KeyCode.X) && nearDrink != ""){
            bool priceCheck = DecCoins(2500);
            if(priceCheck){
                gameObject.transform.GetChild(0).Find("CamOverlay").Find("Buy").GetComponent<UnityEngine.UI.Text>().enabled = false;
                switch(nearDrink){
                    case "JuggerNog": {
                        health = 200;
                        maxHealth = 200;
                        gameObject.transform.GetChild(0).Find("CamOverlay").Find("JuggerNog").GetComponent<UnityEngine.UI.Image>().enabled = true;
                        break;
                    }
                    case "DoubleTap": {
                        gameObject.transform.GetChild(0).Find("CamOverlay").Find("DoubleTap").GetComponent<UnityEngine.UI.Image>().enabled = true;
                        GameObject.Find("Weapon").GetComponent<WaffenScript>().doubleTap(true);
                        break;
                    }
                    case "QuickRevive": {
                        gameObject.transform.GetChild(0).Find("CamOverlay").Find("QuickRevive").GetComponent<UnityEngine.UI.Image>().enabled = true;
                        break;
                    }
                }
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "AmmoBox")
        {
            WaffenScript target = GameObject.Find("Weapon").GetComponent<WaffenScript>();
            if (target != null) target.addMuni(munitionPowerupBonus);
            Destroy(col.gameObject);
        }
        if (col.tag == "Bomb")
        {
            foreach(GameObject zombie in GameObject.FindGameObjectsWithTag("Zombie"))
            {
                ZombieScript zombieScript = zombie.GetComponent<ZombieScript>();
                if(zombieScript != null) zombieScript.TakeDmg(0f ,"Bip01 Head");
            }
            Destroy(col.gameObject);
        }
        if (col.tag == "Instakill")
        {
            WaffenScript target = GameObject.Find("Weapon").GetComponent<WaffenScript>();
            if (target != null) {
                gameObject.transform.GetChild(0).Find("CamOverlay").Find("InstakillImg").GetComponent<UnityEngine.UI.Image>().enabled = true;
                target.Instakill(30f);
                StartCoroutine(showInstakill(20f));
            }
            Destroy(col.gameObject);
        }
        if (col.tag == "Weapon")
        {
            string name = col.transform.name;
            nearWeapon = name.Replace("(Clone)", "");
            isNearWeapon = true;
            if(nearWeapon == activeWeapon) gameObject.transform.GetChild(0).Find("CamOverlay").Find("Buy").GetComponent<UnityEngine.UI.Text>().text = "Drück X - Kaufe Munition für " + nearWeapon;
            else gameObject.transform.GetChild(0).Find("CamOverlay").Find("Buy").GetComponent<UnityEngine.UI.Text>().text = "Drück X - Kaufe " + nearWeapon;
            gameObject.transform.GetChild(0).Find("CamOverlay").Find("Buy").GetComponent<UnityEngine.UI.Text>().enabled = true;
            
        }
        if (col.tag == "RandomWeaponBox")
        {
            RandomWeaponBox = col.transform.parent.gameObject;
            isNearRandomWeaponBox = true;
            if(!RandomWeaponBox.GetComponent<Animation>().IsPlaying("OpenWeaponBox")){
                gameObject.transform.GetChild(0).Find("CamOverlay").Find("Buy").GetComponent<UnityEngine.UI.Text>().text = "Drück X - Öffne Box (1250)";
                gameObject.transform.GetChild(0).Find("CamOverlay").Find("Buy").GetComponent<UnityEngine.UI.Text>().enabled = true;
            }
        }
        if (col.tag == "Drink")
        {
            gameObject.transform.GetChild(0).Find("CamOverlay").Find("Buy").GetComponent<UnityEngine.UI.Text>().text = "Drück X - Drinke " + col.transform.name;
            gameObject.transform.GetChild(0).Find("CamOverlay").Find("Buy").GetComponent<UnityEngine.UI.Text>().enabled = true;
            nearDrink = col.transform.name;
        }
    }

    public IEnumerator showInstakill (float time) {
        WaitForSeconds wait = new WaitForSeconds(time);
        yield return wait;
        gameObject.transform.GetChild(0).Find("CamOverlay").Find("InstakillImg").GetComponent<Animation>().Play("Instakill");
    }

    void OnTriggerExit(Collider col){
        if (col.tag == "Weapon")
        {
            gameObject.transform.GetChild(0).Find("CamOverlay").Find("Buy").GetComponent<UnityEngine.UI.Text>().enabled = false;
            isNearWeapon = false;
        }
        if(col.tag == "RandomWeaponBox"){
            gameObject.transform.GetChild(0).Find("CamOverlay").Find("Buy").GetComponent<UnityEngine.UI.Text>().enabled = false;
            RandomWeaponBox = null;
            isNearRandomWeaponBox = false;
        }
        if (col.tag == "Drink")
        {
            gameObject.transform.GetChild(0).Find("CamOverlay").Find("Buy").GetComponent<UnityEngine.UI.Text>().enabled = false;
            nearDrink = "";
        }
    }

    public void TakeDmgToPlayer(float ammount)
    {
        if (!isDead)
        {
            health -= ammount;
            if (health <= 0 && !isDead) Die();
            else StartCoroutine(DmgTimer(8f));
        }
    }

    public IEnumerator DmgTimer (float time) {
        WaitForSeconds wait = new WaitForSeconds(time);
        yield return wait;
        if (health > 0 && !isDead) health = maxHealth;
    }

    public void AddCoins(bool onHead){
        if(onHead){
            coins += coinsOnHeadshot;
            gameObject.transform.GetChild(0).Find("CamOverlay").Find("AddCoins").GetComponent<UnityEngine.UI.Text>().text = "+ " + coinsOnHeadshot.ToString();
        }else{
            coins += coinsOnHit;
            gameObject.transform.GetChild(0).Find("CamOverlay").Find("AddCoins").GetComponent<UnityEngine.UI.Text>().text = "+ " + coinsOnHit.ToString();
        }
        gameObject.transform.GetChild(0).Find("CamOverlay").Find("AddCoins").GetComponent<Animation>().Play("AddCoins");
        UpdateCoins();
    }

    public bool DecCoins(int amount){
        if(amount > coins) return false;
        else {
            coins -= amount;
            gameObject.transform.GetChild(0).Find("CamOverlay").Find("DecCoins").GetComponent<UnityEngine.UI.Text>().text = "- " + amount.ToString();
            gameObject.transform.GetChild(0).Find("CamOverlay").Find("DecCoins").GetComponent<Animation>().Play("DecCoins");
            UpdateCoins();
            return true;
        }
    }

    void UpdateCoins(){
        gameObject.transform.GetChild(0).Find("CamOverlay").Find("Coins").GetComponent<UnityEngine.UI.Text>().text = coins.ToString();
    }

    void Die()
    {
        gameObject.transform.GetChild(0).Find("CamOverlay").Find("infected").GetComponent<Animation>().Play("infectedAnim");
        gameObject.transform.GetChild(0).Find("CamOverlay").Find("over").GetComponent<Animation>().Play("overAnim");
        gameObject.GetComponent<FirstPersonController>().enabled = false;
        gameObject.transform.GetChild(0).Find("Weapon").Find("WeaponOverlay").GetComponent<Canvas>().enabled = false;
        isDead = true;
    }
}
