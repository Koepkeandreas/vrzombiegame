﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaffenScript : MonoBehaviour
{
    public float magazin = 7f;
    public float maxMagazin = 7f;
    public float gesMuni = 16f;
    public float dmg = 0f;
    public float impactForce = 0f;
    public float fireRate = 0f;
    public float range = 0f;
    public float bulletSize = 9f; 
    public float bulletSpeed = 30f;
    public string activeWeapon = "M1911";

    public Camera fpsCam;
    public ParticleSystem flash;
    public GameObject impactEffect;
    public GameObject bullet;
    public GameObject emtyBullet;
    public GameObject bulletSpawnPoint;
    public GameObject emtyBulletSpawnPoint;
    public GameObject Laser;
    public GameObject Explosion;

    private float nextTimeToFire = 0f;
    private float emtyBulletSpeed = 1.2f;
    private string patronen = "";
    private string maxPatronen = ""; 
    private int price = 0;
    private float ammo = 0;
    private bool doubleTapActive = false;

    public string getActiveWeapon () {
        return activeWeapon;
    }

    void Start()
    {
        gameObject.transform.Find("WeaponOverlay").GetComponent<Canvas>().enabled = true;

        /*GameObject LaserPointer = Instantiate (Laser, transform.position, Quaternion.identity, gameObject.transform) as GameObject;
        LaserPointer.transform.rotation = bulletSpawnPoint.transform.rotation;
        LaserPointer.transform.position = bulletSpawnPoint.transform.position;
        LaserPointer.transform.position = new Vector3(LaserPointer.transform.position.x, LaserPointer.transform.position.y, LaserPointer.transform.position.z+5f);
*/
        gameObject.transform.Find("M1911").gameObject.SetActive(true);
        flash.gameObject.SetActive(true);
        changeWeapon(activeWeapon, false);
        flash.transform.position = new Vector3(flash.transform.position.x-0.05f, flash.transform.position.y+0.02f, flash.transform.position.z);
        bulletSpawnPoint.transform.position = new Vector3(bulletSpawnPoint.transform.position.x-0.05f, bulletSpawnPoint.transform.position.y+0.02f, bulletSpawnPoint.transform.position.z);
        emtyBulletSpawnPoint.transform.position = new Vector3(emtyBulletSpawnPoint.transform.position.x, emtyBulletSpawnPoint.transform.position.y, emtyBulletSpawnPoint.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / fireRate;
            if(magazin > 0f) Shoot();
            if(magazin == 0f && gesMuni > 0f) Debug.Log("Nachladen!");
            if(gesMuni == 0f && magazin == 0f) Debug.Log("Munition ist leer!");
        }

        if (Input.GetButtonDown("Fire2"))
        {
            if(gesMuni > 0f){
                if(magazin < maxMagazin){
                    if(gesMuni >= (maxMagazin - magazin)){
                        gesMuni -= (maxMagazin - magazin);
                        magazin = maxMagazin;
                        patronen = maxPatronen;
                    }else{
                        magazin += gesMuni;
                        gesMuni = 0f;
                        for (float i = 0; i < magazin - 1; i++)
                        {
                            patronen = patronen + "I";
                        }
                    }
                    updateWeaponOverlay();
                    Debug.Log("reload");
                }
            }
        }
    }

    void Shoot()
    {
        //play Shoot animation
        flash.Play();

        if(activeWeapon == "Bennelli_M4"){
            spawnBullets(7);
        }
        else {
            spawnBullets(1);
        }

        //Clone emty bullets
        GameObject emtyBulletClone = Instantiate(emtyBullet, transform.position, transform.rotation) as GameObject;
        emtyBulletClone.transform.localScale = new Vector3(bulletSize / 30, bulletSize / 30, bulletSize / 30);
        //Get the bullet's rigid body component and set its position and rotation equal to that of the spawnPoint
        emtyBulletClone.transform.rotation = emtyBulletSpawnPoint.transform.rotation;
        emtyBulletClone.transform.position = emtyBulletSpawnPoint.transform.position;
        //move forwards 
        Vector3 randomVector = new Vector3(Random.Range(0.4f, 1f), Random.Range(0f, 1f), 0) * emtyBulletSpeed;
        emtyBulletClone.GetComponent<Rigidbody>().velocity = emtyBulletSpawnPoint.transform.TransformDirection(randomVector);
        Destroy(emtyBulletClone, 1f);

        //dec Ammo
        magazin = magazin - 1;
        patronen = patronen.Substring(0, patronen.Length - 1);
        updateWeaponOverlay();
    }

    public void spawnBullets(int bulletAmount){
        for(int i = 1; i <= bulletAmount; i++){
            //Clone bullets
            GameObject bulletClone = Instantiate(bullet, transform.position, transform.rotation) as GameObject;
            bulletClone.transform.localScale = new Vector3(bulletSize / 30, bulletSize / 30, bulletSize / 30);
            //add Script
            bulletClone.AddComponent<Bullet>();
            //call BulletScript
            Bullet script = bulletClone.gameObject.GetComponent<Bullet>();
            if (script != null)
            {
                //script.changeBullet(dmg, range, impactForce, impactEffect, activeWeapon, Explosion);
            }
            //move forwards
            Vector3 vec = Vector3.forward * bulletSpeed;
            if(bulletAmount > 1) vec = (Vector3.forward + new Vector3(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f))) * bulletSpeed;
            bulletClone.GetComponent<Rigidbody>().velocity = bulletSpawnPoint.transform.TransformDirection(vec);
            //Get the bullet's rigid body component and set its position and rotation equal to that of the spawnPoint
            bulletClone.transform.rotation = bulletSpawnPoint.transform.rotation;
            bulletClone.transform.position = bulletSpawnPoint.transform.position;
        }
    }

    public void addMuni(float ammount)
    {
        gesMuni = gesMuni + ammount;
        magazin = maxMagazin;
        patronen = maxPatronen;
        updateWeaponOverlay();
        gameObject.transform.Find("WeaponOverlay").transform.Find("AddMuni").GetComponent<UnityEngine.UI.Text>().text = "+" + ammount.ToString();
        gameObject.transform.Find("WeaponOverlay").transform.Find("AddMuni").GetComponent<Animation>().Play("AddMuniAnimation");
    }

    public void updateWeaponOverlay()
    {
        gameObject.transform.Find("WeaponOverlay").transform.Find("Muni").GetComponent<UnityEngine.UI.Text>().text = magazin.ToString();
        gameObject.transform.Find("WeaponOverlay").transform.Find("gesMuni").GetComponent<UnityEngine.UI.Text>().text = gesMuni.ToString();
        gameObject.transform.Find("WeaponOverlay").transform.Find("Patronen").GetComponent<UnityEngine.UI.Text>().text = patronen;
    }

    public void Instakill (float time){
        StartCoroutine(InstakillTimer(time));
    }

    public IEnumerator InstakillTimer (float time) {
        WaitForSeconds wait = new WaitForSeconds(time);
        float oldDmg = dmg;
        dmg = 1000;
        yield return wait;
        dmg = oldDmg;
    }

    public void changeWeapon(string newWeapon, bool decPrice)
    {
        flash.transform.localPosition = new Vector3(0, 0, 0);
        bulletSpawnPoint.transform.localPosition = new Vector3(0, 0, 0);
        emtyBulletSpawnPoint.transform.localPosition = new Vector3(0, 0, 0);
        
        switch(newWeapon){
            case "Bennelli_M4":{
                dmg = 10f;
                impactForce = 20f;
                fireRate = 1f;
                range = 20f;
                price = 750;
                bulletSize = 9f;
                bulletSpeed = 50f;
                magazin = 7f;
                maxMagazin = 7f;
                ammo = 25;
                flash.transform.localPosition = new Vector3(-0.003f, 0.024f, -0.4f);
                bulletSpawnPoint.transform.localPosition = new Vector3(-0.003f, 0.028f, -0.335f);
                emtyBulletSpawnPoint.transform.localPosition = new Vector3(0, 0.019f, 0.167f);
                break;
            }
            case "AK74":{
                dmg = 25f;
                impactForce = 20f;
                fireRate = 10f;
                range = 100f;
                price = 1750;
                bulletSize = 9f;
                bulletSpeed = 50f;
                magazin = 25f;
                maxMagazin = 25f;
                ammo = 60;
                flash.transform.localPosition = new Vector3(0, 0, -0.225f);
                bulletSpawnPoint.transform.localPosition = new Vector3(0, 0, -0.157f);
                emtyBulletSpawnPoint.transform.localPosition = new Vector3(0, 0.015f, 0.18f);
                break;
            }
            case "M4_8":{
                dmg = 20f;
                impactForce = 10f;
                fireRate = 15f;
                range = 100f;
                price = 1500;
                bulletSize = 9f;
                bulletSpeed = 50f;
                magazin = 30f;
                maxMagazin = 30f;
                ammo = 60;
                flash.transform.localPosition = new Vector3(0, 0, -0.221f);
                bulletSpawnPoint.transform.localPosition = new Vector3(0, 0, -0.137f);
                emtyBulletSpawnPoint.transform.localPosition = new Vector3(0, 0.017f, 0.244f);
                break;
            }
            case "M107":{
                dmg = 100f;
                impactForce = 80f;
                fireRate = 1f;
                range = 200f;
                price = 1500;
                bulletSize = 9f;
                bulletSpeed = 50f;
                magazin = 5f;
                maxMagazin = 5f;
                ammo = 20;
                flash.transform.localPosition = new Vector3(0, -0.018f, -0.648f);
                bulletSpawnPoint.transform.localPosition = new Vector3(0, -0.016f, -0.554f);
                emtyBulletSpawnPoint.transform.localPosition = new Vector3(0, 0, 0.169f);
                break;
            }
            case "M249":{
                dmg = 20f;
                impactForce = 10f;
                fireRate = 20f;
                range = 100f;
                price = 250;
                bulletSize = 9f;
                bulletSpeed = 50f;
                magazin = 60f;
                maxMagazin = 60f;
                ammo = 120;
                flash.transform.localPosition = new Vector3(0.023f, -0.003f, -0.4f);
                bulletSpawnPoint.transform.localPosition = new Vector3(0.02f, -0.008f, -0.29f);
                emtyBulletSpawnPoint.transform.localPosition = new Vector3(0.0125f, 0, 0.17f);
                break;
            }
            case "M1911":{
                dmg = 10f;
                impactForce = 10f;
                fireRate = 3f;
                range = 100f;
                price = 250;
                bulletSize = 9f;
                bulletSpeed = 50f;
                magazin = 7f;
                maxMagazin = 7f;
                ammo = 30;
                flash.transform.localPosition = new Vector3(0, 0.025f, -0.048f);
                bulletSpawnPoint.transform.localPosition = new Vector3(0, 0.026f, 0.045f);
                emtyBulletSpawnPoint.transform.localPosition = new Vector3(0, 0.027f, 0.171f);
                break;
            }
            case "RPG7":{
                dmg = 300f;
                impactForce = 150f;
                fireRate = 0.5f;
                range = 600f;
                price = 2500;
                bulletSize = 60f;
                bulletSpeed = 30f;
                magazin = 1f;
                maxMagazin = 1f;
                ammo = 12;
                flash.transform.localPosition = new Vector3(-0.047f, -0.008f, -0.3f);
                bulletSpawnPoint.transform.localPosition = new Vector3(-0.054f, -0.011f, -0.465f);
                emtyBulletSpawnPoint.transform.localPosition = new Vector3(-0.047f, 0, 0.82f);
                break;
            }
            case "Uzi":{
                dmg = 15f;
                impactForce = 10f;
                fireRate = 20f;
                range = 100f;
                price = 1250;
                bulletSize = 9f;
                bulletSpeed = 50f;
                magazin = 20f;
                maxMagazin = 20f;
                ammo = 60;
                flash.transform.localPosition = new Vector3(-0.003f, 0.027f, -0.044f);
                bulletSpawnPoint.transform.localPosition = new Vector3(0, 0.027f, 0.055f);
                emtyBulletSpawnPoint.transform.localPosition = new Vector3(0, 0.04f, 0.282f);
                break;
            }
        }

        if(doubleTapActive) fireRate = fireRate * 2;

        PlayerScript player = gameObject.transform.root.gameObject.GetComponent<PlayerScript>();
        if(decPrice){
            bool priceCheck = player.DecCoins(price);
            if(priceCheck){
                addMuni(ammo);
                gameObject.transform.Find(activeWeapon).gameObject.SetActive(false);
                gameObject.transform.Find(newWeapon).gameObject.SetActive(true);
                activeWeapon = newWeapon;
            }
        }
        else {
            gameObject.transform.Find(activeWeapon).gameObject.SetActive(false);
            gameObject.transform.Find(newWeapon).gameObject.SetActive(true);
            activeWeapon = newWeapon;
        }
    }

    public void doubleTap (bool active) {
        doubleTapActive = active;
        fireRate = fireRate * 2;
    }
}
