﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class Move : MonoBehaviour
{
    private float m_Gravity = 100.0f;
    private float m_Sensitivity = 2f;
    private float m_Speed = 0.0f;
    private bool isRunning = false;

    public SteamVR_Action_Boolean m_RotatePress = null;
    public SteamVR_Action_Boolean m_MovePress = null;
    public SteamVR_Action_Boolean m_RunPress = null;
    public SteamVR_Action_Vector2 m_MoveValue = null;

    private CharacterController m_CharacterController = null;
    private Transform m_CameraRig = null;
    private Transform m_Head = null;

    private void Awake()
    {
        m_CharacterController = GetComponent<CharacterController>();
    }

    private void Start()
    {
        if(SteamVR_Render.Top()){
            m_CameraRig = SteamVR_Render.Top().origin;
            m_Head = SteamVR_Render.Top().head;  
        }
    }

    private void Update()
    {
        if(m_Head){
            HandleHeight();
            CalculateMovement();

            if (m_RunPress.GetStateDown(SteamVR_Input_Sources.LeftHand))
            {
                if (!isRunning)
                {
                    m_Sensitivity *= 2;
                    isRunning = true;
                }
            }
            if (m_RunPress.GetStateUp(SteamVR_Input_Sources.LeftHand))
            {
                m_Sensitivity = 2f;
                isRunning = false;
            }
        }
    }

    private void HandleHeight()
    {
        // Get the head in local Space
        float headHeight = Mathf.Clamp(m_Head.localPosition.y, 0.1f, 2);
        m_CharacterController.height = headHeight;

        // Cut in half
        Vector3 newCenter = Vector3.zero;
        newCenter.y = m_CharacterController.height / 2;
        newCenter.y += m_CharacterController.skinWidth;

        // Move capsule in local space
        newCenter.x = m_Head.localPosition.x;
        newCenter.z = m_Head.localPosition.z;

        //Apply
        m_CharacterController.center = newCenter;
    }

    private void CalculateMovement()
    {
        // movement orientation
        Vector3 orientationEuler = new Vector3(0.0f, m_Head.eulerAngles.y, 0.0f);
        Quaternion orientation = Quaternion.Euler(orientationEuler);
        Vector3 movement = Vector3.zero;

        // If not moving
        if (m_MovePress.GetStateUp(SteamVR_Input_Sources.Any))
        {
            m_Speed = 0;
        }

        // If button pressed
        if (m_MovePress.state)
        {
            Vector3 moveInput = new Vector3(m_MoveValue.axis.x, 0f, m_MoveValue.axis.y);

            m_Speed = moveInput.sqrMagnitude * m_Sensitivity;

            movement += orientation * (moveInput.normalized * m_Speed);

        }

        // Gravity
        movement.y -= m_Gravity * Time.deltaTime;

        // Apply
        m_CharacterController.Move(movement * Time.deltaTime);

        //sound

    }
}
