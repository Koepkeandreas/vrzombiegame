﻿using UnityEngine;

public class BuyWeaponScript : MonoBehaviour
{
    public GameObject weapon;

    public GameObject spawnWeapon ()
    {
        GameObject weaponClone = Instantiate(weapon, transform.position, transform.rotation) as GameObject;
        weaponClone.transform.name = weaponClone.transform.name.Replace("(Clone)", "");
        return weaponClone;
    }

    public string getWeapon()
    {
        return gameObject.transform.GetChild(1).transform.name;
    }
}
