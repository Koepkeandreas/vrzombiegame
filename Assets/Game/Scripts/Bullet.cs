﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private float dmg = 0f;
    private float range = 0f;
    private float impactForce = 0f;
    private string activeWeapon = "";

    private GameObject impactEffect;
    private GameObject bloodEffect;
    private WaffenScript waffenScript;
    private GameObject Explosion;

    private void Update()
    {
        Destroy(gameObject, range/100f);
    }

    public void changeBullet (float newdmg, float newrange, float newimpactForce, GameObject newimpactEffect, string newactiveWeapon, GameObject ExplosionGO)
    {
        dmg = newdmg;
        range = newrange;
        impactForce = newimpactForce;
        impactEffect = newimpactEffect;
        activeWeapon = newactiveWeapon;
        Explosion = ExplosionGO;
    }

    void doDmg (Collider col, string part) {
        Zombie target = col.transform.root.gameObject.GetComponent<Zombie>();
        if (target != null)
        {
            target.TakeDmg(dmg, part);
        }
    }

    void OnTriggerEnter(Collider col){
        if (col.transform.name != "Player" && col.tag != "Bullet" && col.tag != "Weapon" && col.tag != "GameController" && col.transform.name != "Explosion(Clone)"){
            if(activeWeapon == "RPG7" || activeWeapon == "RPG7Upgrade")
            {
                GameObject explosionClone = Instantiate(Explosion, transform.position, transform.rotation);
                
                Vector3 explosionPos = transform.position;
                Collider[] colliders = Physics.OverlapSphere(explosionPos, 4f);
                foreach (Collider hit in colliders)
                {
                    if(hit.transform.name != "Player" && hit.tag != "Bullet" && hit.transform.name != "Explosion(Clone)"){
                        doDmg(hit, hit.transform.name);

                        Rigidbody rb2 = hit.GetComponent<Rigidbody>();
                        if (rb2 != null) rb2.AddExplosionForce(100f, explosionPos, 4f, 2f);
                    }
                }

                Destroy(explosionClone, 2f);
                Destroy(gameObject);
            }
            else {
                doDmg(col, col.transform.name);

                Rigidbody rb = col.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.AddForce(transform.forward * impactForce);
                }

                GameObject impactGO = Instantiate(impactEffect, transform.position, transform.rotation);
                Destroy(impactGO, 1f);
                Destroy(gameObject);
            }  
        }
    }
}
