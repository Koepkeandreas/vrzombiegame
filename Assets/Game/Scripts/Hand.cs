﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class Hand : MonoBehaviour
{
    public SteamVR_Action_Boolean m_GrabAction = null;
    public SteamVR_Action_Boolean m_BuyPress = null; 
    public Interactable m_CurrentInteractable = null;
    public List<Interactable> m_ContactInteractables = new List<Interactable>();
    public GameObject Ak74Upgrade;
    public GameObject Bennelli_M4Upgrade;
    public GameObject M4_8Upgrade;
    public GameObject M107Upgrade;
    public GameObject M249Upgrade;
    public GameObject M1911Upgrade;
    public GameObject RPG7Upgrade;
    public GameObject UziUpgrade;
    public AudioClip buySound;

    private SteamVR_Behaviour_Pose m_Pose = null;
    private FixedJoint m_Joint = null;
    private float price;
    private float magazin;
    private float maxMagazin;
    private float maxMuni;
    private bool canBuyWeapon = false;
    private bool canBuyGranade = false;
    private string drink = "";
    private bool canBuyDrink = false;
    private bool canUpgrade = false;
    private bool canBuyBarrier = false;
    private GameObject barrier;
    private GameObject weapon;
    private Transform spawnPoint;
    private GameObject upgradeWeapon;
    private bool canOpenRandomWeaponBox = false;
    private GameObject RandomWeaponBox = null;
    private bool isNearRandomWeapon = false;
    private bool throwing = false;

    protected Vector3 m_grabbedObjectPosOff;
    protected Vector3 m_lastPos;
    protected Quaternion m_lastRot;
    protected Quaternion m_grabbedObjectRotOff;
    [SerializeField]
    protected Transform m_gripTransform = null;
    [SerializeField]
    protected OVRInput.Controller m_controller;


    private void Awake()
    {
        m_Pose = GetComponent<SteamVR_Behaviour_Pose>();
        m_Joint = GetComponent<FixedJoint>();
    }

    
    void Update()
    {
        //clean up
        for (var i = m_ContactInteractables.Count - 1; i > -1; i--)
        {
            if (m_ContactInteractables[i] == null)
                m_ContactInteractables.RemoveAt(i);
        }

        // Down
        if (m_GrabAction.GetStateDown(m_Pose.inputSource))
        {
            if (canBuyWeapon)
            {
                m_CurrentInteractable = GetNearestInteractable();
                GameObject weaponClone = m_CurrentInteractable.GetComponent<BuyWeaponScript>().spawnWeapon();
                m_ContactInteractables.Remove(m_CurrentInteractable.gameObject.GetComponent<Interactable>());
                m_CurrentInteractable = null;
                m_ContactInteractables.Add(weaponClone.GetComponent<Interactable>());
                transform.parent.parent.GetComponent<Player>().DecCoins(price, true);
                transform.parent.parent.GetComponent<Player>().AddAmmo(maxMuni);
                GetComponent<AudioSource>().PlayOneShot(buySound);
            }
            if (isNearRandomWeapon && RandomWeaponBox) {
                m_CurrentInteractable = GetNearestInteractable();
                GameObject weaponClone = RandomWeaponBox.GetComponent<RandomWeaponBox>().takeWeapon();
                string weapon = weaponClone.transform.name;
                m_ContactInteractables.Remove(m_CurrentInteractable.gameObject.GetComponent<Interactable>());
                m_CurrentInteractable = null;
                magazin = transform.root.GetComponent<Game>().getWeaponStats(weapon, "magazin");
                maxMagazin = transform.root.GetComponent<Game>().getWeaponStats(weapon, "maxMagazin");
                maxMuni = transform.root.GetComponent<Game>().getWeaponStats(weapon, "maxMuni");
                weaponClone.transform.localScale = new Vector3(1,1,1);
                m_ContactInteractables.Add(weaponClone.GetComponent<Interactable>());
                transform.parent.parent.GetComponent<Player>().AddAmmo(maxMuni);
                isNearRandomWeapon = false;
            }
            Pickup();
        }

        // Up
        if (m_GrabAction.GetStateUp(m_Pose.inputSource))
        {
            Drop();
        }

        // X Button to buy
        if (m_BuyPress.GetStateDown(SteamVR_Input_Sources.Any) && canBuyDrink && drink != "")
        {
            canBuyDrink = false;
            transform.parent.parent.GetComponent<Player>().DecCoins(2500, true);
            transform.parent.parent.GetComponent<Player>().drink(drink);
            transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = false;
        }

        // X Button to Buy Granades
        if (m_BuyPress.GetStateDown(SteamVR_Input_Sources.Any) && canBuyGranade)
        {
            canBuyGranade = false;
            transform.parent.parent.GetComponent<Player>().DecCoins(500, true);
            transform.parent.parent.GetComponent<Player>().AddGranades();
            transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = false;
        }

        // X Button to Open RandomBox
        if (m_BuyPress.GetStateDown(SteamVR_Input_Sources.Any) && canOpenRandomWeaponBox && RandomWeaponBox)
        {
            canOpenRandomWeaponBox = false;
            transform.parent.parent.GetComponent<Player>().DecCoins(1250, true);
            RandomWeaponBox.GetComponent<RandomWeaponBox>().openBox();
            transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = false;
        }

        // X Button to Open Barrier
        if (m_BuyPress.GetStateDown(SteamVR_Input_Sources.Any) && canBuyBarrier && barrier)
        {
            canBuyBarrier = false;
            transform.parent.parent.GetComponent<Player>().DecCoins(float.Parse(barrier.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<UnityEngine.UI.Text>().text), true);
            barrier.GetComponent<BuyBerrier>().open();
            transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = false;
        }

        // X Button to upgrade
        if (m_BuyPress.GetStateDown(SteamVR_Input_Sources.Any) && canUpgrade && weapon && spawnPoint)
        {
            canUpgrade = false;
            transform.parent.parent.GetComponent<Player>().DecCoins(5000, true);
            transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = false;
            switch (weapon.transform.name)
            {
                case "Bennelli_M4":
                    {
                        upgradeWeapon = Instantiate(Bennelli_M4Upgrade, spawnPoint.transform.position, spawnPoint.transform.rotation);
                        break;
                    }
                case "AK74":
                    {
                        upgradeWeapon = Instantiate(Ak74Upgrade, spawnPoint.transform.position, spawnPoint.transform.rotation);
                        break;
                    }
                case "M4_8":
                    {
                        upgradeWeapon = Instantiate(M4_8Upgrade, spawnPoint.transform.position, spawnPoint.transform.rotation);
                        break;
                    }
                case "M107":
                    {
                        upgradeWeapon = Instantiate(M107Upgrade, spawnPoint.transform.position, spawnPoint.transform.rotation);
                        break;
                    }
                case "M249":
                    {
                        upgradeWeapon = Instantiate(M249Upgrade, spawnPoint.transform.position, spawnPoint.transform.rotation);
                        break;
                    }
                case "M1911":
                    {
                        upgradeWeapon = Instantiate(M1911Upgrade, spawnPoint.transform.position, spawnPoint.transform.rotation);
                        break;
                    }
                case "RPG7":
                    {
                        upgradeWeapon = Instantiate(RPG7Upgrade, spawnPoint.transform.position, spawnPoint.transform.rotation);
                        break;
                    }
                case "Uzi":
                    {
                        upgradeWeapon = Instantiate(UziUpgrade, spawnPoint.transform.position, spawnPoint.transform.rotation);
                        break;
                    }
            }
            upgradeWeapon.transform.name = upgradeWeapon.transform.name.Replace("(Clone)", "");
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if(collider.transform.name.Contains("(RandomWeaponBox)")){
            isNearRandomWeapon = true;
        }
        if (collider.gameObject.CompareTag("Interactable") || collider.gameObject.CompareTag("Weapon") || collider.gameObject.CompareTag("Granate"))
        {
            m_ContactInteractables.Add(collider.gameObject.GetComponent<Interactable>());
        }
        if (collider.gameObject.CompareTag("BuyWeapon") && collider.GetComponent<BuyWeaponScript>())
        {
            // get price from wepon
            string weapon = collider.GetComponent<BuyWeaponScript>().getWeapon();
            price = transform.root.GetComponent<Game>().getWeaponStats(weapon, "price");
            magazin = transform.root.GetComponent<Game>().getWeaponStats(weapon, "magazin");
            maxMagazin = transform.root.GetComponent<Game>().getWeaponStats(weapon, "maxMagazin");
            maxMuni = transform.root.GetComponent<Game>().getWeaponStats(weapon, "maxMuni");
            // priceCheck
            if (transform.parent.parent.GetComponent<Player>().DecCoins(price, false))
            {
                m_ContactInteractables.Add(collider.gameObject.GetComponent<Interactable>());
                canBuyWeapon = true;
            }
        }
        if (collider.gameObject.CompareTag("Drink"))
        {
            drink = collider.gameObject.transform.name;
            if (transform.parent.parent.GetComponent<Player>().GetDrink(collider.transform.name))
            {
                transform.parent.Find("Controller (left)").Find("Buy").Find("Text").GetComponent<UnityEngine.UI.Text>().text = "Du hast " + drink + " schon!";
                transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = true;
            }
            else
            {
                if (transform.parent.parent.GetComponent<Player>().DecCoins(2500, false))
                {
                    canBuyDrink = true;
                    transform.parent.Find("Controller (left)").Find("Buy").Find("Text").GetComponent<UnityEngine.UI.Text>().text = "Trinke " + drink + " für 2500 Coins.";
                    transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = true;
                }
                else
                {
                    transform.parent.Find("Controller (left)").Find("Buy").Find("Text").GetComponent<UnityEngine.UI.Text>().text = "Für " + drink + " brauchst du 2500 Coins!";
                    transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = true;
                }
            }
        }
        if (collider.gameObject.CompareTag("Upgrade") && transform.parent.Find("Controller (right)").GetComponent<Hand>().m_CurrentInteractable)
        {
            if (transform.parent.parent.GetComponent<Player>().DecCoins(5000, false))
            {
                spawnPoint = collider.transform.Find("SpawnPoint");
                weapon = transform.parent.Find("Controller (right)").GetComponent<Hand>().m_CurrentInteractable.gameObject;
                canUpgrade = true;
                transform.parent.Find("Controller (left)").Find("Buy").Find("Text").GetComponent<UnityEngine.UI.Text>().text = "Upgrade " + weapon.transform.name + " für 5000 Coins.";
                transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = true;
            }
            else
            {
                transform.parent.Find("Controller (left)").Find("Buy").Find("Text").GetComponent<UnityEngine.UI.Text>().text = "Zum Upgraden brauchst du 5000 Coins!";
                transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = true;
            }
        }
        if(collider.gameObject.CompareTag("BuyGranade")){
            if (transform.parent.parent.GetComponent<Player>().GetGranades() == 3f)
            {
                transform.parent.Find("Controller (left)").Find("Buy").Find("Text").GetComponent<UnityEngine.UI.Text>().text = "Du hast schon 3 Granaten!";
                transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = true;
            }
            else
            {
                if (transform.parent.parent.GetComponent<Player>().DecCoins(500, false))
                {
                    canBuyGranade = true;
                    transform.parent.Find("Controller (left)").Find("Buy").Find("Text").GetComponent<UnityEngine.UI.Text>().text = "Kaufe 3 Granaten für 500 Coins.";
                    transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = true;
                }
                else
                {
                    transform.parent.Find("Controller (left)").Find("Buy").Find("Text").GetComponent<UnityEngine.UI.Text>().text = "Du brauchst 500 Coins!";
                    transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = true;
                }
            }
        }
        if(collider.gameObject.CompareTag("RandomWeaponBox")){
            if (!collider.GetComponent<Animation>().IsPlaying("OpenWeaponBox"))
            {
                if (transform.parent.parent.GetComponent<Player>().DecCoins(1250, false))
                {
                    canOpenRandomWeaponBox = true;
                    RandomWeaponBox = collider.gameObject;
                    transform.parent.Find("Controller (left)").Find("Buy").Find("Text").GetComponent<UnityEngine.UI.Text>().text = "Öffne die Box für 1250 Coins.";
                    transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = true;
                }
                else
                {
                    transform.parent.Find("Controller (left)").Find("Buy").Find("Text").GetComponent<UnityEngine.UI.Text>().text = "Du brauchst 1250 Coins!";
                    transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = true;
                }
            }
        }
        if (collider.gameObject.CompareTag("Barrier"))
        {
            barrier = collider.gameObject;
            if (transform.parent.parent.GetComponent<Player>().DecCoins(float.Parse(barrier.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<UnityEngine.UI.Text>().text), false))
            {
                canBuyBarrier = true;
                transform.parent.Find("Controller (left)").Find("Buy").Find("Text").GetComponent<UnityEngine.UI.Text>().text = "Öffne Barriere für "+ barrier.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<UnityEngine.UI.Text>().text + " Coins.";
                transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = true;
            }
            else
            {
                transform.parent.Find("Controller (left)").Find("Buy").Find("Text").GetComponent<UnityEngine.UI.Text>().text = "Zum öffnen der Barriere brauchst du " + barrier.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<UnityEngine.UI.Text>().text + " Coins!";
                transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = true;
            }
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if(collider.transform.name.Contains("(RandomWeaponBox)")){
            isNearRandomWeapon = false;
        }
        if (collider.gameObject.CompareTag("Interactable") || collider.gameObject.CompareTag("Weapon") || collider.gameObject.CompareTag("Granate"))
        {
            m_ContactInteractables.Remove(collider.gameObject.GetComponent<Interactable>());
        }
        if (collider.gameObject.CompareTag("BuyWeapon"))
        {
            m_ContactInteractables.Remove(collider.gameObject.GetComponent<Interactable>());
            canBuyWeapon = false;
        }
        if (collider.gameObject.CompareTag("Drink"))
        {
            drink = "";
            canBuyDrink = false;
            transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = false;
        }
        if (collider.gameObject.CompareTag("Upgrade"))
        {
            weapon = null;
            canUpgrade = false;
            transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = false;
        }
        if (collider.gameObject.CompareTag("BuyGranade"))
        {
            m_ContactInteractables.Remove(collider.gameObject.GetComponent<Interactable>());
            transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = false;
            canBuyGranade = false;
        }
        if(collider.gameObject.CompareTag("RandomWeaponBox")){
            canOpenRandomWeaponBox = false;
            transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = false;
        }
        if (collider.gameObject.CompareTag("Barrier"))
        {
            canBuyBarrier = false;
            transform.parent.Find("Controller (left)").Find("Buy").GetComponent<Canvas>().enabled = false;
            barrier = null;
        }
    }

    public void Pickup()
    {
        // Get the nearest
        m_CurrentInteractable = GetNearestInteractable();

        // Null check
        if (!m_CurrentInteractable)
        {
            return;
        }

        if(m_CurrentInteractable.tag == "Granate"){
            m_CurrentInteractable.GetComponent<Rigidbody>().useGravity = true;
            m_CurrentInteractable.GetComponent<Rigidbody>().isKinematic = false;
        }

        if (m_CurrentInteractable.transform.name.Contains("Upgrade"))
        {
            m_CurrentInteractable.gameObject.GetComponent<Rigidbody>().isKinematic = false;
            m_CurrentInteractable.gameObject.GetComponent<Rigidbody>().useGravity = true;
        }

        // Already held check
        if (m_CurrentInteractable.m_ActiveHand)
        {
            m_CurrentInteractable.m_ActiveHand.Drop();
        }

        // Position & Rotation
        m_lastPos = transform.position;
        m_lastRot = transform.rotation;

        if (m_CurrentInteractable.snapPosition)
        {
            m_grabbedObjectPosOff = m_gripTransform.localPosition;
            if (m_CurrentInteractable.snapOffset)
            {
                Vector3 snapOffset = m_CurrentInteractable.snapOffset.position;
                if (m_controller == OVRInput.Controller.LTouch) snapOffset.x = -snapOffset.x;
                m_grabbedObjectPosOff += snapOffset;
            }
        }
        else
        {
            Vector3 relPos = m_CurrentInteractable.transform.position - transform.position;
            relPos = Quaternion.Inverse(transform.rotation) * relPos;
            m_grabbedObjectPosOff = relPos;
        }

        if (m_CurrentInteractable.snapOrientation)
        {
            m_grabbedObjectRotOff = m_gripTransform.localRotation;
            if (m_CurrentInteractable.snapOffset)
            {
                m_grabbedObjectRotOff = m_CurrentInteractable.snapOffset.rotation * m_grabbedObjectRotOff;
            }
        }
        else
        {
            Quaternion relOri = Quaternion.Inverse(transform.rotation) * m_CurrentInteractable.transform.rotation;
            m_grabbedObjectRotOff = relOri;
        }


        Rigidbody grabbedRigidbody = m_CurrentInteractable.GetComponent<Rigidbody>();
        Vector3 grabbablePosition = m_lastPos + m_lastRot * m_grabbedObjectPosOff;
        Quaternion grabbableRotation = m_lastRot * m_grabbedObjectRotOff;

        grabbedRigidbody.transform.position = grabbablePosition;
        grabbedRigidbody.transform.rotation = grabbableRotation;

        // Attach
        m_Joint.connectedBody = grabbedRigidbody;

        // Set active hand
        m_CurrentInteractable.m_ActiveHand = this;

        //check if weapon
        if (m_CurrentInteractable.tag == "Weapon")
        {
            m_CurrentInteractable.transform.SetParent(gameObject.transform.parent.Find("Weapon"));
            transform.parent.parent.GetComponent<Player>().changeWeaponStats(magazin, maxMagazin, m_CurrentInteractable.transform.name.Contains("Upgrade"));
            transform.parent.parent.GetComponent<Player>().showAmmoOverlay();
        }
    }

    public void Drop()
    {
        // Null check
        if (!m_CurrentInteractable)
        {
            return;
        }

        //check if weapon
        if (m_CurrentInteractable.tag == "Weapon")
        {
            transform.parent.parent.GetComponent<Player>().hideAmmoOverlay();
            m_CurrentInteractable.transform.SetParent(GameObject.Find("Map").transform);
        }

        // Apply velocity
        Rigidbody targetBody = m_CurrentInteractable.GetComponent<Rigidbody>();
        targetBody.velocity = m_Pose.GetVelocity();
        targetBody.angularVelocity = m_Pose.GetAngularVelocity();

        m_ContactInteractables.Remove(m_CurrentInteractable.gameObject.GetComponent<Interactable>());

        // Detach
        m_Joint.connectedBody = null;

        //Destroy after 2 sec
        if (m_CurrentInteractable.tag != "Weapon" && m_CurrentInteractable.gameObject.transform.name != "Magazin" && m_CurrentInteractable.gameObject.transform.name != "Rakete" && m_CurrentInteractable.tag != "Granate") Destroy(m_CurrentInteractable.gameObject, 2f);

        // If Granade, call Explosion
        if(m_CurrentInteractable.tag == "Granate"){
            m_CurrentInteractable.GetComponent<Rigidbody>().AddForce(m_CurrentInteractable.gameObject.transform.forward * 600f);
            m_CurrentInteractable.GetComponent<GranadeScript>().startTimer();
        }

        // Clear
        m_CurrentInteractable.m_ActiveHand = null;
        m_CurrentInteractable = null;
    }

    private Interactable GetNearestInteractable()
    {
        Interactable nearest = null;
        float minDistance = float.MaxValue;
        float distance = 0.0f;

        foreach(Interactable interactable in m_ContactInteractables)
        {
            distance = (interactable.transform.position - transform.position).sqrMagnitude;

            if(distance < minDistance)
            {
                minDistance = distance;
                nearest = interactable;
            }
        }

        return nearest;
    }

    public Interactable getGrabbeldObject()
    {
        return m_CurrentInteractable;
    }
}
