﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagazinScript : MonoBehaviour
{
    public Transform magazinPoint;
    public AudioClip reloadSound;

    private GameObject magazin;
    private bool moveMagazin = false;

    private void Update()
    {
        if (magazin && moveMagazin)
        {
            if (!GetComponent<AudioSource>().isPlaying)
            {
                GetComponent<AudioSource>().PlayOneShot(reloadSound);
            }
            if (Vector3.Distance(magazin.transform.position, magazinPoint.position) > 0.001f)
            {
                magazin.transform.position = Vector3.MoveTowards(magazin.transform.position, magazinPoint.position, Time.deltaTime);
                magazin.transform.rotation = magazinPoint.transform.rotation;
            }
            if (Vector3.Distance(magazin.transform.position, magazinPoint.position) <= 0.001f)
            {
                moveMagazin = false;
                magazin.transform.SetParent(gameObject.transform.parent);
                GameObject.Find("Player").GetComponent<Player>().reload(true);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.name == "Magazin" || other.transform.name == "Rakete")
        {
            other.gameObject.GetComponent<BoxCollider>().enabled = false;
            other.gameObject.GetComponent<Rigidbody>().useGravity = false;
            other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            magazin = other.gameObject;
            moveMagazin = true;
        }
    }
}
