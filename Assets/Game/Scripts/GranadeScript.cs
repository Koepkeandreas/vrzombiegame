﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GranadeScript : MonoBehaviour
{
    public GameObject Explosion;
    private float explosionTime = 5f;
    private float dmg = 200f;
    public void startTimer () {
        StartCoroutine(explode(explosionTime));
    }

    private IEnumerator explode(float time)
    {
        WaitForSeconds wait = new WaitForSeconds(time);
        yield return wait;

        GameObject explosionClone = Instantiate(Explosion, transform.position, transform.rotation);
                
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, 4f);
        foreach (Collider hit in colliders)
        {
            if(hit.transform.name != "Player" && hit.tag != "Bullet" && hit.transform.name != "Explosion(Clone)"){
                doDmg(hit, hit.transform.name);

                Rigidbody rb2 = hit.GetComponent<Rigidbody>();
                if (rb2 != null) rb2.AddExplosionForce(100f, explosionPos, 4f, 2f);
            }
        }

        Destroy(explosionClone, 2f);
        Destroy(gameObject);
    }

    void doDmg (Collider col, string part) {
        Zombie target = col.transform.root.gameObject.GetComponent<Zombie>();
        if (target != null)
        {
            target.TakeDmg(dmg, part);
        }
    }
}
