﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public Transform[] ZombieSpawners;
    public GameObject Zombie;
    public AudioClip newRoundSound;

    private float maxZombieSpawn = 50f;
    private float zombiesPerWave = 5f;
    private float wave = 1f;
    private bool waveIsLoading = true;
    private float zombieanz = 0f;
    private float spawnedZombies = 0f;

    private float price;
    private float maxMuni;
    private float magazin;
    private float maxMagazin;

    void Start()
    {
        if(Zombie && ZombieSpawners.Length > 0) startGame();
    }

    void Update()
    { 
        zombieanz = GameObject.FindGameObjectsWithTag("Zombie").Length;

        if (Zombie && ZombieSpawners.Length > 0 && GameObject.Find("Zombie(Clone)") == null && !waveIsLoading)
        {
            waveIsLoading = true;
            spawnedZombies = 0f;
            StartCoroutine(newWave());
        }
    }

    public void startGame()
    {
        showWave();
        StartCoroutine(spawnZombies());

    }

    public IEnumerator newWave()
    {
        WaitForSeconds wait = new WaitForSeconds(10f);
        yield return wait;
        GetComponent<AudioSource>().PlayOneShot(newRoundSound);
        wave++;
        showWave();
        StartCoroutine(spawnZombies());
    }

    public IEnumerator spawnZombies()
    {
        for(;zombieanz < maxZombieSpawn && spawnedZombies < wave * zombiesPerWave;){
            int randomSpawner = Mathf.RoundToInt(Random.Range(0f, ZombieSpawners.Length - 1f));
            GameObject ZombieClone = Instantiate(Zombie, ZombieSpawners[randomSpawner].transform.position, ZombieSpawners[randomSpawner].transform.rotation) as GameObject;
            ZombieClone.SetActive(true);
            float randomFloat = Random.Range(0.9f, 1.1f);
            ZombieClone.transform.localScale = new Vector3(randomFloat, randomFloat, randomFloat);
            spawnedZombies++;
            if (spawnedZombies == wave * zombiesPerWave) waveIsLoading = false;
            yield return new WaitForSeconds(Random.Range(1f, 4f));
        }
        if(waveIsLoading && spawnedZombies < wave * zombiesPerWave){
            yield return new WaitForSeconds(3f);
            StartCoroutine(spawnZombies());
        }
    }

    public void showWave()
    {
        if(transform.GetChild(0).Find("Camera (eye)")){
            transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("Wave").GetComponent<UnityEngine.UI.Text>().text = "Wave " + wave.ToString();
            transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("Wave").GetComponent<Animation>().Play("schowWave");
        }
    }

    public float getWeaponStats(string weapon, string value)
    {
        switch (weapon)
        {
            case "Bennelli_M4":
                {
                    price = 1500;
                    magazin = 6f;
                    maxMagazin = 6f;
                    maxMuni = 54f;
                    break;
                }
            case "AK74":
                {
                    price = 1750;
                    magazin = 25f;
                    maxMagazin = 25f;
                    maxMuni = 170f;
                    break;
                }
            case "M4_8":
                {
                    price = 1500;
                    magazin = 30f;
                    maxMagazin = 30f;
                    maxMuni = 210f;
                    break;
                }
            case "M107":
                {
                    price = 1500;
                    magazin = 5f;
                    maxMagazin = 5f;
                    maxMuni = 20f;
                    break;
                }
            case "M249":
                {
                    price = 3500;
                    magazin = 60f;
                    maxMagazin = 60f;
                    maxMuni = 350f;
                    break;
                }
            case "M1911":
                {
                    price = 250;
                    magazin = 8f;
                    maxMagazin = 8f;
                    maxMuni = 32f;
                    break;
                }
            case "RPG7":
                {
                    price = 3500;
                    magazin = 1f;
                    maxMagazin = 1f;
                    maxMuni = 20f;
                    break;
                }
            case "Uzi":
                {
                    price = 1000;
                    magazin = 24f;
                    maxMagazin = 24f;
                    maxMuni = 120f;
                    break;
                }
        }

        switch (value)
        {
            case "price":
                return price;
            case "magazin":
                return magazin;
            case "maxMagazin":
                return maxMagazin;
            case "maxMuni":
                return maxMuni;
        }

        return 0;
    }
}
