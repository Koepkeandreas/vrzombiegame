﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMagazinScript : MonoBehaviour
{
    private GameObject magazinClone;

    public void spawnMagazin(GameObject magazin)
    {
        string weapon = transform.parent.Find("Weapon").GetChild(0).transform.name;
        if (weapon == "RPG7" || weapon == "RPG7Upgrade") magazin.SetActive(true);
        magazinClone = Instantiate(magazin, transform.position, transform.rotation) as GameObject;
        if (weapon == "RPG7" || weapon == "RPG7Upgrade") Destroy(magazin);
        magazinClone.gameObject.transform.name = magazinClone.transform.name.Replace("(Clone)", "");
        magazinClone.GetComponent<BoxCollider>().enabled = true;
    }

    public void destroyMagazin()
    {
        if (magazinClone)  Destroy(magazinClone);
    }
}
