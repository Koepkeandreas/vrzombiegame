﻿using UnityEngine;

public class Zombie : MonoBehaviour
{
    public Camera Player;
    public GameObject[] Powerups;
    public GameObject[] Parts;
    public AudioClip[] screamSounds;
    public AudioClip[] attackSounds;
    public AudioClip dieSound;

    private float dmg = 50;
    private float health = 200f;
    //declare the transform of our goal (where the navmesh agent will move towards) and our navmesh agent (in this case our zombie)
    private Transform goal;
    private UnityEngine.AI.NavMeshAgent agent;
    private bool isDead = false;
    private float attackRange = 2.3f;
    private float nextTimeToSound = 0f;
    private bool playSound = true;

    void Start () {
        int randomPart = Mathf.RoundToInt(Random.Range(0f, Parts.Length-1f));
        switch(Parts[randomPart].transform.name){
            case "L Forearm": {
                transform.Find("L Hand").gameObject.SetActive(false);
                break;
            }
            case "L UpperArm": {
                transform.Find("L Hand").gameObject.SetActive(false);
                transform.Find("L Forearm").gameObject.SetActive(false);
                break;
            }
            case "L Spine1": {
                transform.Find("L Hand").gameObject.SetActive(false);
                transform.Find("L Forearm").gameObject.SetActive(false);
                transform.Find("L UpperArm").gameObject.SetActive(false);
                break;
            }
            case "R Forearm": {
                transform.Find("R Hand").gameObject.SetActive(false);
                break;
            }
            case "R UpperArm": {
                transform.Find("R Hand").gameObject.SetActive(false);
                transform.Find("R Forearm").gameObject.SetActive(false);
                break;
            }
            case "R Spine1": {
                transform.Find("R Hand").gameObject.SetActive(false);
                transform.Find("R Forearm").gameObject.SetActive(false);
                transform.Find("R UpperArm").gameObject.SetActive(false);
                break;
            }
        }
        Parts[randomPart].SetActive(false);
    }

    void Update()
    {
        if (!isDead)
        {
            float dist = Vector3.Distance(Player.transform.position, gameObject.transform.position);
            if (dist <= attackRange && !GetComponent<Animation>().IsPlaying("attack")) attack();
            if (dist > attackRange && !GetComponent<Animation>().IsPlaying("attack")) walk();
        }
        if (!GetComponent<AudioSource>().isPlaying && playSound)
        {
            if (GetComponent<Animation>().IsPlaying("walk_in_place") && Time.time >= nextTimeToSound)
            {
                int randomSound = Mathf.RoundToInt(Random.Range(0f, screamSounds.Length - 1f));
                GetComponent<AudioSource>().PlayOneShot(screamSounds[randomSound]);
                nextTimeToSound = Time.time + 1f / Random.Range(0.05f, 0.7f);
            }
            if (GetComponent<Animation>().IsPlaying("attack"))
            {
                int randomSound = Mathf.RoundToInt(Random.Range(0f, attackSounds.Length - 1f));
                GetComponent<AudioSource>().PlayOneShot(attackSounds[randomSound]);
            }
            if (isDead)
            {
                GetComponent<AudioSource>().PlayOneShot(dieSound);
                playSound = false;
            }
        }
    }

    void walk()
    {
        //create references
        goal = Player.transform;
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        //set the navmesh agent's desination equal to the main camera's position (our first person character)
        agent.destination = goal.position;
        //start the walking animation
        GetComponent<Animation>().Play("walk_in_place");
    }

    void attack()
    {
        //stop the zombie from moving forward by setting its destination to it's current position
        agent.destination = gameObject.transform.position;
        GetComponent<Animation>().Stop();
        //stop the walking animation and play attack animation
        GetComponent<Animation>().Play("attack");
        //do Dmg to Player
        Player player = Player.transform.parent.parent.gameObject.GetComponent<Player>();
        if (player != null)
        {
            player.TakeDmgByZombie(dmg);
        }
    }

    public void TakeDmg(float amount, string part)
    {
        Player player = Player.transform.parent.parent.gameObject.GetComponent<Player>();
        if (player != null && !isDead) player.AddCoins(part == "Bip01 Head");

        if (part == "Bip01 Head") health = 0;
        else health -= amount;

        if (health <= 0 && !isDead) Die();
    }

    void Die()
    {
        //stop the walking animation and play attack animation
        GetComponent<Animation>().Stop();
        isDead = true;
        //stop the zombie from moving forward by setting its destination to it's current position
        agent.destination = gameObject.transform.position;
        //chance to drop a Powerup
        if (Random.value > 0.9) //%100 percent chance (1 - 0.2 is 0.8)
        {
            Vector3 pos = transform.position;
            pos.y += 1f;
            int randomPowerup = Mathf.RoundToInt(Random.Range(0f, Powerups.Length - 1f));
            Instantiate(Powerups[randomPowerup], pos, transform.rotation);
        }
        //destroy dead Body after 4 sec
        Destroy(gameObject, 4f);
    }
}