﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupScript : MonoBehaviour
{
    private void Update()
    {
        if (Vector3.Distance(GameObject.FindGameObjectWithTag("Player").transform.position, gameObject.transform.position) <= 2.7f)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().powerup(gameObject);
            Destroy(gameObject);
        }
    }
}
