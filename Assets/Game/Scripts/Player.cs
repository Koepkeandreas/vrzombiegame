﻿using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float health = 100f;
    private float maxHealth = 100f;
    private float HealTime = 5f;
    private bool isDead = false;
    private float coins = 50000;
    private float coinsOnHit = 10;
    private float coinsOnHeadshot = 60;
    private float munitionPowerupBonus = 50f;
    private float InstakillTime = 20f;
    private bool secondLife = false;

    // Weaponstats
    private float magazin;
    private float gesMuni;
    private float maxMagazin;
    private string patronen = "";
    private string maxPatronen = "";
    private float maxMuni = 50f;
    private float granades = 0f;

    //Drinks
    private bool JuggerNog = false;
    private bool DoubleTap = false;
    private bool QuickRevive = false;

    void Start()
    {
        UpdateCoins();
    }

    public void powerup(GameObject powerup)
    {
        switch (powerup.tag)
        {
            case "AmmoBox":
                {
                    AddAmmo(maxMuni);
                    break;
                }
            case "Bomb":
                {
                    foreach (GameObject zombie in GameObject.FindGameObjectsWithTag("Zombie"))
                    {
                        ZombieScript zombieScript = zombie.GetComponent<ZombieScript>();
                        if (zombieScript != null) zombieScript.TakeDmg(0f, "Bip01 Head");
                    }
                    break;
                }
            case "Instakill":
                {
                    transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("Instakill").GetComponent<UnityEngine.UI.Image>().enabled = true;
                    StartCoroutine(showInstakill(InstakillTime));
                    break;
                }
        }
    }

    public float GetGranades () {
        return granades;
    }

    public void AddGranades () {
        granades = 3f;
    }

    public void DecGranades () {
        granades--;
    }

    public IEnumerator showInstakill(float time)
    {
        WaitForSeconds wait = new WaitForSeconds(time);
        yield return wait;
        if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("Instakill").GetComponent<Animation>().Play("Instakill");
    }

    public void TakeDmgByZombie(float amount)
    {
        if (!isDead)
        {
            health -= amount;
            if (health <= 0 && !isDead) Die();
            else StartCoroutine(HealTimer(HealTime));
        }
    }

    public IEnumerator HealTimer(float time)
    {
        WaitForSeconds wait = new WaitForSeconds(time);
        yield return wait;
        if (health > 0 && !isDead) health = maxHealth;
    }

    public bool reload(bool instant)
    {
        if (gesMuni == 0f) return false;
        else
        {
            magazin = 0f;
            patronen = "";
            if (instant)
            {
                if (gesMuni < maxMagazin)
                {
                    magazin = gesMuni;
                    gesMuni = 0f;
                    for (float i = 0; i < magazin - 1; i++)
                    {
                        patronen = patronen + "I";
                    }
                }
                else
                {
                    gesMuni = gesMuni - maxMagazin;
                    magazin = maxMagazin;
                    patronen = maxPatronen;
                }
            }
            updateAmmoOverlay();
            return true;
        }
    }

    public bool shoot(bool instant)
    {
        if (magazin <= 0f) return false;
        else
        {
            if (instant)
            {
                magazin = magazin - 1;
                patronen = patronen.Substring(0, patronen.Length - 1);
                updateAmmoOverlay();
            }
            return true;
        }
    }

    public void AddAmmo(float amount)
    {
        maxMuni = amount;
        gesMuni = maxMuni;
        if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("MunitionView").Find("AddMuni").GetComponent<UnityEngine.UI.Text>().text = "+" + amount.ToString();
        if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("MunitionView").Find("AddMuni").GetComponent<Animation>().Play("AddMuniAnimation");
        updateAmmoOverlay();
    }

    public void changeWeaponStats(float newMagazin, float newMaxMuni, bool upgrade)
    {
        magazin = newMagazin;
        maxMagazin = newMaxMuni;

        if (upgrade) AddAmmo(maxMuni * 2);

        createPatronText();
    }

    public void showAmmoOverlay ()
    {
        if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("MunitionView").gameObject.SetActive(true);
    }

    public void hideAmmoOverlay()
    {
        if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("MunitionView").gameObject.SetActive(false);
    }

    public void updateAmmoOverlay()
    {
        if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("MunitionView").Find("Muni").GetComponent<UnityEngine.UI.Text>().text = magazin.ToString();
        if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("MunitionView").Find("gesMuni").GetComponent<UnityEngine.UI.Text>().text = gesMuni.ToString();
        if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("MunitionView").Find("Patronen").GetComponent<UnityEngine.UI.Text>().text = patronen;
    }

    public void createPatronText()
    {
        patronen = "";
        maxPatronen = "";

        for (float i = 0; i < magazin; i++)
        {
            patronen = patronen + "I";
        }

        updateAmmoOverlay();

        for (float i = 0; i < maxMagazin; i++)
        {
            maxPatronen = maxPatronen + "I";
        }
    }

    public void AddCoins(bool onHead)
    {
        if (onHead)
        {
            coins += coinsOnHeadshot;
            if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("CoinView").Find("AddCoins").GetComponent<UnityEngine.UI.Text>().text = "+ " + coinsOnHeadshot.ToString();
        }
        else
        {
            coins += coinsOnHit;
            if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("CoinView").Find("AddCoins").GetComponent<UnityEngine.UI.Text>().text = "+ " + coinsOnHit.ToString();
        }
        UpdateCoins();

        GameObject addCoins = transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("CoinView").Find("AddCoins").gameObject;
        GameObject coinsClone = Instantiate(addCoins, addCoins.transform.position, addCoins.transform.rotation) as GameObject;
        coinsClone.transform.SetParent(transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("CoinView"));
        coinsClone.GetComponent<Animation>().Play("AddCoins");
        Destroy(coinsClone, 1f);
    }

    public bool DecCoins(float amount, bool buyInstant)
    {
        if (amount > coins) return false;
        if (buyInstant)
        {
            coins -= amount;
            if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("CoinView").Find("DecCoins").GetComponent<UnityEngine.UI.Text>().text = "- " + amount.ToString();
            if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("CoinView").Find("DecCoins").GetComponent<Animation>().Play("DecCoins");
            UpdateCoins();
        }
        return true;
    }

    void UpdateCoins()
    {
        if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("CoinView").Find("Coins").GetComponent<UnityEngine.UI.Text>().text = coins.ToString();
    }

    public void drink(string drink)
    {
        switch (drink)
        {
            case "JuggerNog":
                {
                    health = 200;
                    maxHealth = 200;
                    if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("DrinkView").Find("JuggerNog").GetComponent<UnityEngine.UI.Image>().enabled = true;
                    JuggerNog = true;
                    break;
                }
            case "DoubleTap":
                {
                    if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("DrinkView").Find("DoubleTap").GetComponent<UnityEngine.UI.Image>().enabled = true;
                    if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Controller (right)").GetComponent<ShootScript>().activateDoubleTap(true);
                    DoubleTap = true;
                    break;
                }
            case "QuickRevive":
                {
                    if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("DrinkView").Find("QuickRevive").GetComponent<UnityEngine.UI.Image>().enabled = true;
                    secondLife = true;
                    QuickRevive = true;
                    break;
                }
        }
    }

    public bool GetDrink (string drink)
    {
        switch (drink)
        {
            case "JuggerNog":
                {
                    return JuggerNog;
                }
            case "DoubleTap":
                {
                    return DoubleTap;
                }
            case "QuickRevive":
                {
                    return QuickRevive;
                }
            default:
                {
                    return false;
                }
        }
    }

    void Die()
    {
        if (secondLife)
        {
            health = 100;
            maxHealth = 100;
            if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("DrinkView").Find("JuggerNog").GetComponent<UnityEngine.UI.Image>().enabled = false;
            JuggerNog = false;
            if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("DrinkView").Find("DoubleTap").GetComponent<UnityEngine.UI.Image>().enabled = false;
            transform.GetChild(0).Find("Controller (right)").GetComponent<ShootScript>().activateDoubleTap(false);
            DoubleTap = false;
            if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("View").Find("DrinkView").Find("QuickRevive").GetComponent<UnityEngine.UI.Image>().enabled = false;
            secondLife = false;
            QuickRevive = false;
            transform.GetChild(0).Find("Controller (right)").GetComponent<Hand>().Drop();
            foreach (GameObject zombie in GameObject.FindGameObjectsWithTag("Zombie"))
            {
                if (Vector3.Distance(zombie.transform.position, transform.position) <= 8f)
                {
                    Zombie zombieScript = zombie.GetComponent<Zombie>();
                    if (zombieScript != null) zombieScript.TakeDmg(0f, "Bip01 Head");
                }
            }
            coins += 500;
}   
        else
        {
            if(transform.GetChild(0).Find("Camera (eye)")) transform.GetChild(0).Find("Camera (eye)").GetChild(0).Find("GameOver").GetComponent<UnityEngine.UI.Text>().enabled = true;
            isDead = true;
        }
    }
}
